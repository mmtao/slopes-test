#define BOOST_TEST_MODULE slope - test

// For a slope calculation explanation and to understand the pupil naming scheme, see:
// "Real-time adaptive optics with pyramid wavefront sensors: part I. A theoretical analysis of the pyramid sensor model."
// https://iopscience.iop.org/article/10.1088/1361-6420/ab0656.

#include <ipp.h>
#include <limits.h>
#include <sys/time.h>

// Accumulators in Boost 1.81.00 uses some elements deprecated in newer C++ standards.
// Let's pretend that it doesn't.
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wdeprecated-copy"

#include <array>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/format.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/version.hpp>

#include "slopes.h"
#include "slopes1.h"
#include "slopes2.h"

// Number of cycles to measure over.
#define CYCLES 100000

// Inputs and outputs.
static frame_t frame_buffer __attribute__((aligned(64)));
static slopes_t slopes_output __attribute__((aligned(64)));

// Fill the frame buffer with random pixel values.
template <typename S, typename T, long unsigned int N>
void fill(std::array<S, N> &arr, boost::random::uniform_real_distribution<T> &dist, boost::mt19937 &gen) {
  for (unsigned int i = 0; i < N; i++) {
    arr[i] = dist(gen);
  }
}

// A timestamp.
inline double timestamp() {
  static double ten_to_the_nine = 1000000000.0;
  static struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  double retval = t.tv_nsec;
  retval /= ten_to_the_nine;
  retval += t.tv_sec;
  return retval;
}

BOOST_AUTO_TEST_CASE(test1) {
  std::cout << "-------------------" << std::endl
            << boost::format("Test 1: Naive C++, Boost %s, GCC %s") % BOOST_LIB_VERSION % __VERSION__ << std::endl;
  std::cout << "Processor         : " << std::flush;
  system("lscpu | grep 'Model name' | sed -e 's/Model name:[ ]*//'");

  // Make a random number generator. Use the 16th Bell number (https://oeis.org/A000110) as a seed.
  // Then, make a uniform distribution to pull pixel numbers from.
  boost::mt19937 gen(1382958545);
  boost::random::uniform_real_distribution<float> dist(0, SHRT_MAX);

  // Get ready record statistics about performance.
  boost::accumulators::accumulator_set<double,                                                          //
                                       boost::accumulators::stats<boost::accumulators::tag::mean,       //
                                                                  boost::accumulators::tag::max,        //
                                                                  boost::accumulators::tag::min,        //
                                                                  boost::accumulators::tag::variance>>  //
      loopStats = {};

  double u, v, norm;
  compute_slopes_1 cs1(&frame_buffer, &slopes_output, &norm);
  for (int i = 0; i < CYCLES; i++) {
    // Set up the iteration.
    fill(frame_buffer, dist, gen);
    pupil_set_t ps = {{84, 63, 16}, {83, 97, 16}, {48, 62, 16}, {48, 98, 16}};

    u = timestamp();

    // Do the work.
    cs1.compute(ps);

    // Record stats.
    v = timestamp();
    loopStats(v - u);
  }

  std::cout << boost::format("avg (min/max/var) : %g (%g, %g, %g) seconds") % boost::accumulators::mean(loopStats) %
                   boost::accumulators::min(loopStats) % boost::accumulators::max(loopStats) % boost::accumulators::variance(loopStats)
            << std::endl;
}

BOOST_AUTO_TEST_CASE(test2) {
  std::cout << "-------------------" << std::endl
            << boost::format("Test 2: Intel IPP %s, Boost %s, GCC %s") % IPP_VERSION_STR % BOOST_LIB_VERSION % __VERSION__ << std::endl;
  std::cout << "Processor         : " << std::flush;
  system("lscpu | grep 'Model name' | sed -e 's/Model name:[ ]*//'");

  // Make a random number generator. Use the 16th Bell number (https://oeis.org/A000110) as a seed.
  // Then, make a uniform distribution to pull pixel numbers from.
  boost::mt19937 gen(1382958545);
  boost::random::uniform_real_distribution<float> dist(0, SHRT_MAX);

  // Get ready record statistics about performance.
  boost::accumulators::accumulator_set<double,                                                          //
                                       boost::accumulators::stats<boost::accumulators::tag::mean,       //
                                                                  boost::accumulators::tag::max,        //
                                                                  boost::accumulators::tag::min,        //
                                                                  boost::accumulators::tag::variance>>  //
      loopStats = {};

  double u, v, norm;
  compute_slopes_2 cs2(&frame_buffer, &slopes_output, &norm);
  for (int i = 0; i < CYCLES; i++) {
    // Set up the iteration.
    fill(frame_buffer, dist, gen);
    pupil_set_t ps = {{84, 63, 16}, {83, 97, 16}, {48, 62, 16}, {48, 98, 16}};

    u = timestamp();

    // Do the work.
    cs2.compute(ps);

    // Record stats.
    v = timestamp();
    loopStats(v - u);
  }

  std::cout << boost::format("avg (min/max/var) : %g (%g, %g, %g) seconds") % boost::accumulators::mean(loopStats) %
                   boost::accumulators::min(loopStats) % boost::accumulators::max(loopStats) % boost::accumulators::variance(loopStats)
            << std::endl;
}
