#include "slopes1.h"

void compute_slopes_1::extract_pupil(uint16_t row, uint16_t col, uint16_t rad, frame_t* img, uint16_t* out) {
  // Go to the top left corner of the pupil.
  row -= rad;
  col -= rad;

  // Copy a square out.
  for (int r = 0; r < rad * 2; r++) {
    uint16_t* inp = img->data() + ((row + r) * WIDTH);
    for (int c = 0; c < rad * 2; c++) {
      *out++ = inp[col + c];
    }
  }
}

// These functions are noinlined to make it easier to read the generated assembly.

static void __attribute__ ((noinline)) sum16u(uint16_t* inp, int num, double* sum) {
  double s = 0.0f;
  for (int i = 0; i < num; i++) {
    s += *inp++;
  }
  *sum = s;
}

static void __attribute__ ((noinline)) convert_u16_f(uint16_t* inp, float* out, int num) {
  for (int i = 0; i < num; i++) {
    out[i] = inp[i];
  }
}

static void __attribute__ ((noinline)) add_block(float* in1, float* in2, float* out, int n) {
  for (int i = 0; i < n; i++) {
    out[i] = in1[i] + in2[i];
  }
}

static void __attribute__ ((noinline)) sub_block(float* in1, float* in2, float* out, int n) {
  for (int i = 0; i < n; i++) {
    out[i] = in1[i] - in2[i];
  }
}

static void __attribute__ ((noinline)) divc_block(float* inp, float* out, float c, int n) {
  for (int i = 0; i < n; i++) {
    out[i] = inp[i] / c;
  }
}

static void __attribute__ ((noinline)) copy_block(float* inp, float* out, int rows, int cols, int row_elems, int row_offset) {
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      out[c + row_elems * r + row_offset] = *inp++;
    }
  }
}

void compute_slopes_1::compute(pupil_set_t& ps) {
  // Step 1. Extract pupils.
  extract_pupil(ps.p01.row, ps.p01.col, SLOPE_PUPIL_RAD, inp, cI01);
  extract_pupil(ps.p11.row, ps.p11.col, SLOPE_PUPIL_RAD, inp, cI11);
  extract_pupil(ps.p00.row, ps.p00.col, SLOPE_PUPIL_RAD, inp, cI00);
  extract_pupil(ps.p10.row, ps.p10.col, SLOPE_PUPIL_RAD, inp, cI10);

  // Step 2. Find the norm by summing pixels.
  // TODO: Use a mask to include only pupil pixels in the rectangular region.
  double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0, num_p = SLOPE_PUPIL_DIAM * SLOPE_PUPIL_DIAM, n;
  sum16u(cI01, num_p, &sum1);
  sum16u(cI11, num_p, &sum2);
  sum16u(cI00, num_p, &sum3);
  sum16u(cI10, num_p, &sum4);
  n = (sum1 + sum2 + sum3 + sum4) / (4 * num_p);
  *norm = n;

  // Step 3. Convert the pixels to floats.
  convert_u16_f(cI00, fI00, num_p);
  convert_u16_f(cI01, fI01, num_p);
  convert_u16_f(cI10, fI10, num_p);
  convert_u16_f(cI11, fI11, num_p);

  // Step 4. Slopes.

  // Step 4.1. Addition.
  add_block(fI01, fI00, A, num_p);  // A = I01 + I00.
  add_block(fI11, fI10, B, num_p);  // B = I11 + I10.
  add_block(fI01, fI11, C, num_p);  // C = I01 + I11.
  add_block(fI00, fI10, D, num_p);  // D = I00 + I10.

  // Step 4.2. Subtraction.
  sub_block(A, B, E, num_p);  // E = A - B.
  sub_block(C, D, F, num_p);  // F = C - D.

  // Step 4.3. Division.
  divc_block(E, sx, n, num_p);  // sx = E / norm.
  divc_block(F, sy, n, num_p);  // sy = F / norm.

  // Step 5. Assemble slopes.
  copy_block(sx, out->data(), SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM, 2 * SLOPE_PUPIL_DIAM, 0);
  copy_block(sx, out->data(), SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM, 2 * SLOPE_PUPIL_DIAM, 0);
}