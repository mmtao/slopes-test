#include "slopes2.h"

void compute_slopes_2::extract_pupil(uint16_t row, uint16_t col, uint16_t rad, frame_t* img, uint16_t* out) {
  IppiSize dims = {SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM};
  row -= rad;
  col -= rad;
  ippiCopy_16u_C1R(img->data() + col + row * 160, 160 * 2, out, SLOPE_PUPIL_DIAM * 2, dims);
}

void compute_slopes_2::compute(pupil_set_t& ps) {
  // Step 1. Extract pupils.
  extract_pupil(ps.p01.row, ps.p01.col, SLOPE_PUPIL_RAD, inp, cI01);
  extract_pupil(ps.p11.row, ps.p11.col, SLOPE_PUPIL_RAD, inp, cI11);
  extract_pupil(ps.p00.row, ps.p00.col, SLOPE_PUPIL_RAD, inp, cI00);
  extract_pupil(ps.p10.row, ps.p10.col, SLOPE_PUPIL_RAD, inp, cI10);

  // Step 2. Find the norm by summing pixels.
  // TODO: Use a mask to include only pupil pixels in the rectangular region.
  double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0, num_p = SLOPE_PUPIL_DIAM * SLOPE_PUPIL_DIAM;
  ippiSum_16u_C1R(cI01, hstep, dimsSlopes, &sum1);
  ippiSum_16u_C1R(cI11, hstep, dimsSlopes, &sum2);
  ippiSum_16u_C1R(cI00, hstep, dimsSlopes, &sum3);
  ippiSum_16u_C1R(cI10, hstep, dimsSlopes, &sum4);
  *norm = (sum1 + sum2 + sum3 + sum4) / (4 * num_p);

  // Step 3. Convert pixels to floats.
  ippiConvert_16u32f_C1R(cI00, hstep, fI00, fstep, dimsSlopes);
  ippiConvert_16u32f_C1R(cI01, hstep, fI01, fstep, dimsSlopes);
  ippiConvert_16u32f_C1R(cI10, hstep, fI10, fstep, dimsSlopes);
  ippiConvert_16u32f_C1R(cI11, hstep, fI11, fstep, dimsSlopes);

  // Step 4. Slopes.

  // Step 4.1. Addition.
  ippiAdd_32f_C1R(fI01, fstep, fI00, fstep, A, fstep, dimsSlopes);  // A = I01 + I00.
  ippiAdd_32f_C1R(fI11, fstep, fI10, fstep, B, fstep, dimsSlopes);  // B = I11 + I10.
  ippiAdd_32f_C1R(fI01, fstep, fI11, fstep, C, fstep, dimsSlopes);  // C = I01 + I11.
  ippiAdd_32f_C1R(fI00, fstep, fI10, fstep, D, fstep, dimsSlopes);  // D = I00 + I10.

  // Step 4.2. Subtraction.
  ippiSub_32f_C1R(A, fstep, B, fstep, E, fstep, dimsSlopes);  // E = A - B.
  ippiSub_32f_C1R(C, fstep, D, fstep, F, fstep, dimsSlopes);  // F = C - D.

  // Step 4.3. Division.
  ippiDivC_32f_C1R(E, fstep, *norm, sx, fstep, dimsSlopes);  // sx = E / norm.
  ippiDivC_32f_C1R(F, fstep, *norm, sy, fstep, dimsSlopes);  // sy = F / norm.

  // Step 5. Assemble slopes.
  ippiCopy_32f_C1R(sx, SLOPE_PUPIL_DIAM * sizeof(float), out->data(), SLOPE_PUPIL_DIAM * 2 * sizeof(float), dimsSlopes);
  ippiCopy_32f_C1R(sy, SLOPE_PUPIL_DIAM * sizeof(float), out->data() + SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM * 2 * sizeof(float), dimsSlopes);
}