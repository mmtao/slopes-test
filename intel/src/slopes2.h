#pragma once

#include <ipp.h>

#include <array>

#include "slopes.h"

class compute_slopes_2 {
 private:
  frame_t* inp = nullptr;
  slopes_t* out = nullptr;
  double* norm = nullptr;

  // IPP setup for slopes.
  IppiSize dimsSlopes = {SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM};
  int fstep = SLOPE_PUPIL_DIAM * 4;  // float step.
  int hstep = SLOPE_PUPIL_DIAM * 2;  // halfword step.
  int bstep = SLOPE_PUPIL_DIAM * 1;  // byte step.

// An unoptimized set of buffers to do the slope calculation. Since so many are the same
// size, it should be possible to reuse them. That said, the data are small enough that it
// isn't worth the effort yet to do this.
#define BUF_SIZE (SLOPE_PUPIL_DIAM * SLOPE_PUPIL_DIAM)
  uint16_t cI00[BUF_SIZE] __attribute__((aligned(64))),  //
      cI01[BUF_SIZE] __attribute__((aligned(64))),       //
      cI10[BUF_SIZE] __attribute__((aligned(64))),       //
      cI11[BUF_SIZE] __attribute__((aligned(64)));
  float fI00[BUF_SIZE] __attribute__((aligned(64))),  //
      fI01[BUF_SIZE] __attribute__((aligned(64))),    //
      fI10[BUF_SIZE] __attribute__((aligned(64))),    //
      fI11[BUF_SIZE] __attribute__((aligned(64))),    //
      sx[BUF_SIZE] __attribute__((aligned(64))),      //
      sy[BUF_SIZE] __attribute__((aligned(64))),      //
      A[BUF_SIZE] __attribute__((aligned(64))),       //
      B[BUF_SIZE] __attribute__((aligned(64))),       //
      C[BUF_SIZE] __attribute__((aligned(64))),       //
      D[BUF_SIZE] __attribute__((aligned(64))),       //
      E[BUF_SIZE] __attribute__((aligned(64))),       //
      F[BUF_SIZE] __attribute__((aligned(64)));

  // Extract pupils from the image given their locations.
  void extract_pupil(uint16_t row, uint16_t col, uint16_t rad, frame_t* img, uint16_t* out);

 public:
  // Ctor and setup.
  compute_slopes_2(frame_t* frame, slopes_t* slopes, double* norm) : inp(frame), out(slopes), norm(norm) {}

  // Do the whole computation.
  void compute(pupil_set_t& ps);
};
