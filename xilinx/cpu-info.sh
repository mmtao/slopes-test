#!/bin/bash

VENDOR=`lscpu | grep "^Vendor " | sed -e 's/.*:[ ]*//'`
MODEL=`lscpu | grep "^Model " | sed -e 's/.*:[ ]*//'`
CORES=`lscpu | grep '^CPU(s)' | sed -e 's/.*:[ ]*//'`
MHZ=`lscpu | grep 'CPU max' | sed -e 's/.*:[ ]*//' -e 's/\..*//'`
BOARD=`cat /proc/device-tree/model | sed -e 's/\x00/\n/'`

echo $BOARD $VENDOR $MODEL x$CORES @ $MHZ MHz max