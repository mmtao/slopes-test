#include "slopes2.h"

void compute_slopes_2::extract_pupil(uint16_t row, uint16_t col, uint16_t rad, frame_t* img, uint16_t* out) {
  // Go to the top left corner of the pupil.
  row -= rad;
  col -= rad;

  // Copy a square out.
  for (int r = 0; r < rad * 2; r++) {
    uint16_t* inp = img->data() + ((row + r) * WIDTH);
    for (int c = 0; c < rad * 2; c++) {
      *out++ = inp[col + c];
    }
  }
}

// These functions are noinlined to make it easier to read the generated assembly.

#define PROCEDURE static void __attribute__((noinline))

PROCEDURE sum16u(uint16_t* inp, int num, double* sum) {
  uint32_t s = 0;
  for (int i = 0; i < num; i++) {
    s += *inp++;
  }
  *sum = s;
}

PROCEDURE convert_and_add_blocks(uint16_t* __restrict__ p01,  //
                                 uint16_t* __restrict__ p11,  //
                                 uint16_t* __restrict__ p00,  //
                                 uint16_t* __restrict__ p10,  //
                                 float* __restrict__ A,       // A = I01 + I00.
                                 float* __restrict__ B,       // B = I11 + I10.
                                 float* __restrict__ C,       // C = I01 + I11.
                                 float* __restrict__ D        // D = I00 + I10.
) {
  for (int i = 0; i < SLOPE_PUPIL_DIAM * SLOPE_PUPIL_DIAM; i++) {
    float f01 = p01[i], f11 = p11[i], f00 = p00[i], f10 = p10[i];
    A[i] = f01 + f00;
    B[i] = f11 + f10;
    C[i] = f01 + f11;
    D[i] = f00 + f10;
  }
}

PROCEDURE sub_and_divc_blocks(float* __restrict__ A,  //
                              float* __restrict__ B,  //
                              float* __restrict__ E,  // E = (A - B) / norm
                              float* __restrict__ C,  //
                              float* __restrict__ D,  //
                              float* __restrict__ F,  // F = (C - D) / norm
                              float norm              //
) {
  float one_over_norm = 1.0f / norm;
  for (int i = 0; i < 36 * 36; i++) {
    E[i] = (A[i] - B[i]) * one_over_norm;
    F[i] = (C[i] - D[i]) * one_over_norm;
  }
}

PROCEDURE
copy_block(float* __restrict__ inp, float* __restrict__ out, int rows, int cols, int row_elems, int row_offset) {
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      out[c + row_elems * r + row_offset] = *inp++;
    }
  }
}

void compute_slopes_2::compute(pupil_set_t& ps) {
  // Step 1. Extract pupils.
  extract_pupil(ps.p01.row, ps.p01.col, SLOPE_PUPIL_RAD, inp, cI01);
  extract_pupil(ps.p11.row, ps.p11.col, SLOPE_PUPIL_RAD, inp, cI11);
  extract_pupil(ps.p00.row, ps.p00.col, SLOPE_PUPIL_RAD, inp, cI00);
  extract_pupil(ps.p10.row, ps.p10.col, SLOPE_PUPIL_RAD, inp, cI10);

  // Step 2. Find the norm by summing pixels.
  // TODO: Use a mask to include only pupil pixels in the rectangular region.
  double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0, num_p = SLOPE_PUPIL_DIAM * SLOPE_PUPIL_DIAM, n;
  sum16u(cI01, num_p, &sum1);
  sum16u(cI11, num_p, &sum2);
  sum16u(cI00, num_p, &sum3);
  sum16u(cI10, num_p, &sum4);
  n = (sum1 + sum2 + sum3 + sum4) / (4 * num_p);
  *norm = n;

  // Step 3. Convert the pixels to floats.
  // Was: convert_u16_f(cI00, fI00); convert_u16_f(cI01, fI01); convert_u16_f(cI10, fI10); convert_u16_f(cI11, fI11);

  // Step 4. Slopes.

  // Step 4.1. Addition.
  // Was: add_block(cI01, cI00, A); add_block(cI11, cI10, B); add_block(cI01, cI11, C); add_block(cI00, cI10, D);
  convert_and_add_blocks(cI01, cI11, cI00, cI10, A, B, C, D);

  // Step 4.2. Subtraction. Was: sub_block(A, B, E); sub_block(C, D, F);
  // Step 4.3. Division. Was: divc_block(E, sx, n); divc_block(F, sy, n);
  sub_and_divc_blocks(A, B, E, C, D, F, n);

  // Step 5. Assemble slopes.
  copy_block(sx, out->data(), SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM, 2 * SLOPE_PUPIL_DIAM, 0);
  copy_block(sy, out->data(), SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM, 2 * SLOPE_PUPIL_DIAM, SLOPE_PUPIL_DIAM);
}