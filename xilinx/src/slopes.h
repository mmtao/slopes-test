#pragma once

#include <array>
#include <cstdint>

// Meta-data about the pupils.
typedef struct {
  typedef struct {
    int row, col, rad;
  } pupil_meta_t;
  pupil_meta_t p00, p01, p10, p11;
  bool is_valid() { return (p00.rad > 0 && p01.rad > 0 && p10.rad > 0 && p11.rad > 0); }
} pupil_set_t;

// Use CCID75 sensor dimensions and fixed slopes dimensions from MAPS.
#define WIDTH 160
#define HEIGHT 128
#define SLOPES_WIDTH 72
#define SLOPES_HEIGHT 36
#define SLOPE_PUPIL_DIAM 36
#define SLOPE_PUPIL_RAD 18
#define FSTEP (SLOPE_PUPIL_DIAM * 4)
#define HSTEP (SLOPE_PUPIL_DIAM * 2)
#define BSTEP (SLOPE_PUPIL_DIAM * 1)

typedef std::array<uint16_t, WIDTH * HEIGHT> frame_t;
typedef std::array<float, SLOPES_WIDTH * SLOPES_HEIGHT> slopes_t;